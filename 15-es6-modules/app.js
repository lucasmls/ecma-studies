// import [method] form [libary]
// * carrega tudo da lib
// as => alias para o método (novo nome)

// import { union } from 'ramda';  [importação apenas do union]
// import { union as juntaTudo, uniq as soOsMesmo } from 'ramda';
import sum, { sub, mult, div } from './utils';

import * as R from 'ramda';
import react from 'react';
import ReactDOM from 'react-dom';

const arr1 = [1, 1, 1, 2, 2, 3, 4, 5, 6, 6];
const arr2 = [5, 6, 6, 6, 7, 7, 8, 9, 10, 1];

const arr3 = R.union(arr1, arr2);
console.log(arr3);

console.log(sum(2, 10));
console.log(sub(10, 5));
console.log(mult(10, 5));
console.log(div(10, 5));

