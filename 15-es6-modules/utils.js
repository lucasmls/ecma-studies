function sum(a, b) {
  return a + b;
}


//named export
// vários exports por script
// importado utilizando { name }
export function sub(a, b) {
  return a - b;
}

function mult(a, b) {
  return a * b;
}

function div(a, b) {
  return a / b;
}
export { mult, div };

// método principal
// apenas um default por script
export default sum;

// export * as utils;